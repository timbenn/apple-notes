import React, { useState } from 'react';
import styles from './App.css';
import { throttle } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import uuid from 'react-uuid';

function App() {
    const [notes, updateNotes] = useState([]);
    const [filteredNotes, updateFilteredNotes] = useState([]);
    const [search, updateSearch] = useState('');
    const [note, updateNote] = useState(null);

    const addNote = (note) => {
        var updatedNotes = [note, ...notes];
        updateNotes(updatedNotes);
        updateFilteredNotes(updatedNotes);
        updateNote(note);
    };

    var filter = () => {
        var updatedList = notes;
        updatedList = updatedList.filter(note => {
            var isSearchInTitle = note.title.toUpperCase().search(search.toUpperCase()) !== -1;
            var isSearchInBody = note.body.toUpperCase().search(search.toUpperCase()) !== -1;
            return isSearchInTitle || isSearchInBody;
        })
    
        updateFilteredNotes(updatedList);
    };

    const filterNotes = throttle(filter, 1000);

    const handleSearchChange = value => {
        updateSearch(value);
        filterNotes();
    };

    const handleEnterPress = (e) => {
        if (e.key === 'Enter' && search !== '') {
            addNote({
                id: uuid(),
                title: search,
                body: '',
                createdAt: moment(),
                isSelected: true
            });
            
            handleSearchChange('');
        }
    }

    return (
        <div className="App">
            <div className={ styles.header }>
                <button><FontAwesomeIcon icon={ faTrash } /></button>
                <button><FontAwesomeIcon icon={ faEdit } /></button>
                <input
                    value={ search }
                    onChange={ (e) => handleSearchChange(e.target.value) }
                    onKeyPress={ (e) => handleEnterPress(e) }
                    placeholder="Search" />
            </div>
            <main>
                <div className={ styles['notes-container'] }>
                    { filteredNotes.length > 0 ? 
                        filteredNotes.map(note => {
                            return <div key={ note.id } className={ styles.note } onClick={ () => updateNote(note) }>
                                <h3 className={ styles.title }>{ note.title }</h3>
                                <div className={ styles['date-message-container'] }>
                                    <span className={ styles.date }>{ moment(note.createdAt).format('h:mm A') }</span>
                                    <p className={ styles.body }>{ note.body }</p>
                                </div>
                            </div>
                        }) : <div className={ styles['no-notes'] }>
                            <h3>You don't have any notes...yet!</h3>
                        </div> }
                </div>
                <div className={ styles['note-container'] }>
                    { note ? 
                        <div>
                            <div className={ styles['date-container'] }>
                                <p className={ styles.date }>{ moment(note.createdAt).format('MMMM Do, YYYY [at] h:mm A') }</p>
                            </div>
                            {/* <textarea className={ styles.body } value={ note.body } onChange={ (e) => handleChange(e) }></textarea> */}
                        </div>
                    : null }
                </div>
            </main>
        </div>
    );
}

export default App;

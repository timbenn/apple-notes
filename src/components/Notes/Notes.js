import React from 'react';
import styles from './Notes.module.css';
import moment from 'moment';

function Notes(props) {
    return (
        <div className={ styles['notes-container'] }>
            { props.notes.length > 0 ? 
                props.notes.map(note => {
                    return <div key={ note.id } className={ styles.note } onClick={ () => props.select(note) }>
                        <h3 className={ styles.title }>{ note.title }</h3>
                        <div className={ styles['date-message-container'] }>
                            <span className={ styles.date }>{ moment(note.createdAt).format('h:mm A') }</span>
                            <p className={ styles.body }>{ note.body }</p>
                        </div>
                    </div>
                }) : <div className={ styles['no-notes'] }>
                    <h3>You don't have any notes...yet!</h3>
                </div> }
        </div>
    )
}

export default Notes;

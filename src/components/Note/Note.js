import React, { useState } from 'react';
import styles from './Note.module.css';
import moment from 'moment';

function Note(props) {
    const [body, setBody] = useState(props.note.body);

    var handleChange = (event) => {
        setBody(event.target.value);
        props.note.body = body;
        props.updateNote(props.note);
    };

    return (
        <div className={ styles['note-container'] }>
            { props.note ? 
                <div>
                    <div className={ styles['date-container'] }>
                        <p className={ styles.date }>{ moment(props.note.createdAt).format('MMMM Do, YYYY [at] h:mm A') }</p>
                    </div>
                    <textarea className={ styles.body } value={ body } onChange={ (e) => handleChange(e) }></textarea>
                </div>
            : null }
            
        </div>
    )
}

export default Note;

import React from 'react';
import styles from './Header.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import uuid from 'react-uuid';
import { debounce } from 'lodash';

function Header(props) {
    var handleChange = debounce((value) => {
        props.filterNotes(value)
    }, 500);

    return (
        <div className={ styles.header }>
            <button>
                <FontAwesomeIcon icon={ faTrash } />
            </button>
            <button>
                <FontAwesomeIcon icon={ faEdit } />
            </button>
            <input
                onChange={ (e) => handleChange(e.target.value) }
                onKeyPress={ (e) => {
                        if (e.key === 'Enter' && e.target.value !== '') {
                            props.addNote({
                                id: uuid(),
                                title: e.target.value,
                                body: '', createdAt: moment(),
                                isSelected: true
                            });
                            
                            e.target.value = '';
                        }
                    }
                }
                placeholder="Search" />
        </div>
    )
}

export default Header;
